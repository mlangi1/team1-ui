import { InjectionToken } from '@angular/core';

export interface AppConfiguration {
  env: string;
  baseURL: string;
  production: boolean;
}

export const AppConfig = new InjectionToken<AppConfiguration>(
  '@@appConfiguration'
);

export const BaseAPIURL = new InjectionToken<string>('@@appBaseAPIURL');
