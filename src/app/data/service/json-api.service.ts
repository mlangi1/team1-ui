import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import claimGroups from './json/claim-groups.json';
import claimsDetail from './json/claim-details.json';

@Injectable({
  providedIn: 'root'
})
export class JsonApiService {
  get(url: string): Observable<any> {
    switch (url) {
      case '/claim-groups':
        return of(claimGroups['claim-groups']);
      case '/claim-detail':
        return of(claimsDetail);
      default:
        const id = url.substring(url.lastIndexOf('/') + 1);
        return of(claimGroups['claim-groups'][id]);
    }
  }
}
