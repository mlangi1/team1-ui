import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';

import { ClaimGroup } from '../schema/claim-group';
import { JsonApiService } from './json-api.service';
import { BaseAPIURL } from '../../../configuration';
import { ClaimDetails } from '@data/schema/claim-details.interface';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClaimsService {
  constructor(
    private http: HttpClient,
    private jsonApiService: JsonApiService,
    @Inject(BaseAPIURL) private readonly apiURL
  ) {}

  getAllClaimGroups(): Observable<Array<ClaimGroup>> {
    return this.jsonApiService.get('/claim-groups');
  }

  getSingleClaimGroup(id: number): Observable<ClaimGroup> {
    return this.jsonApiService.get('/claim-groups/' + id);
  }
  getClaimData(claimId: number): Observable<ClaimDetails> {
    // use for mock testing for now
    //return this.jsonApiService.get('/claim-detail');
    return this.http.get<ClaimDetails>(
      `${this.apiURL}/third-party-liability?claimNumber=P028OHE00565`
    );
  }
}
