export interface EobImage {
    blobUrl: string,
    contentType: string
}