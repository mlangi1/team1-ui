export class ClaimGroup {
  CLAIM_COUNT: number;
  CLAIM_DATE_RECEIVED: string;
  CLAIM_CHARGE_TOTAL: number;
  CLAIM_TYPE: string;
  MEMBER_ID: string;
}
