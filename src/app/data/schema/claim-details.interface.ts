export interface ClaimDetails {
  claimNumber: string;
  memberNumber: string;
  carrier: string;
  serviceLines: ServiceLine[];
  coordinationOfBenefits: CoordinationOfBenefit[];
  members: Member[];
}

export interface ServiceLine {
  serviceLine: string;
  serviceNumber: string;
  procedures: string[];
  amount: number;
  startDate: string;
  endDate: string;
  allow: number;
  pay: number;
  exArray: string[];
  pendInformation: PendInfomration[];
}
export interface PendInfomration {
  sequenceNbr: number;
  carc: string;
  carcDesc: string;
  code: string;
  desc: string;
}
export interface CoordinationOfBenefit {
  altCarrierName: string;
  altPolicyNbr: string;
  effectiveDt: string;
  endDt: string;
  primacyOrder: string;
}

export interface Member {
  businessLine: string;
  addressLine1: string;
  addressLine2: string;
  countyName: string;
  state: string;
  zipCode: string;
  medicareID: string;
  medicaidID: string;
  ssn: string;
  amisysID: string;
  mpi: string;
  firstName: string;
  lastName: string;
  businessUnit: string;
  gender: string;
}

export interface ServiceLineTable {
  serviceLines: ServiceLine[];
  product: string;
}
