import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { tap, delay, finalize, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

import { AuthService } from '@app/service/auth.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { authCodeFlowConfig } from 'app/auth-code-flow.config';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(private oauthService: OAuthService) {}

  ngOnInit() {
    // this.oauthService.initImplicitFlow();
  }

  login() {
    this.oauthService.configure(authCodeFlowConfig);
    this.oauthService.loadDiscoveryDocument();
    sessionStorage.setItem('flow', 'code');

    this.oauthService.initLoginFlow();
    // this.isLoading = true;
    // const credentials = this.loginForm.value;
    // this.authService
    //   .login(credentials)
    //   .pipe(
    //     delay(5000),
    //     tap((user) => this.router.navigate(['/dashboard/home'])),
    //     finalize(() => (this.isLoading = false)),
    //     catchError((error) => of((this.error = error)))
    //   )
    //   .subscribe();
  }
}
