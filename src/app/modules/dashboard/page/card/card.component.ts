import { Component, OnInit, Input } from '@angular/core';
import { ClaimGroup } from '@data/schema/claim-group';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() claimGroup: ClaimGroup;
  constructor() {}

  ngOnInit(): void {}
}
