import { RouterTestingModule } from '@angular/router/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardComponent } from './card.component';
import { SharedModule } from '@shared/shared.module';
import { ClaimGroup } from '@data/schema/claim-group';

describe('CardComponent', () => {
  let component: CardComponent;
  let fixture: ComponentFixture<CardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CardComponent],
      imports: [SharedModule, RouterTestingModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardComponent);
    component = fixture.componentInstance;
    component.claimGroup = {
      CLAIM_COUNT: 2,
      CLAIM_DATE_RECEIVED: '10 03 2019',
      CLAIM_CHARGE_TOTAL: 11111.11,
      CLAIM_TYPE: 'Type',
      MEMBER_ID: 'U123456789'
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
