import { MatButtonToggle } from '@angular/material/button-toggle';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-change-view',
  templateUrl: './change-view.component.html',
  styleUrls: ['./change-view.component.scss']
})
export class ChangeViewComponent implements OnInit {
  selectedView: string = 'grid';
  @Output() selectEvent = new EventEmitter<string>();

  constructor() {}

  ngOnInit(): void {}

  selectView(view: MatButtonToggle) {
    this.selectedView = view.value;
    this.selectEvent.emit(this.selectedView);
  }
}
