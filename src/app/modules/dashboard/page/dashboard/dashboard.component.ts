import { Component, OnInit } from '@angular/core';

import { ClaimsService } from '@data/service/claims.service';
import { ClaimGroup } from '@data/schema/claim-group';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  claimGroups: ClaimGroup[];
  selectedView: string = 'grid';

  constructor(private claimsService: ClaimsService) {}

  ngOnInit(): void {
    this.loadClaims();
  }

  async loadClaims() {
    this.claimGroups = await this.claimsService.getAllClaimGroups().toPromise();
  }

  selectView($event) {
    this.selectedView = $event;
  }
}
