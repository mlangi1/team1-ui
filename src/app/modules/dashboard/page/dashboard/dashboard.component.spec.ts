import { CardComponent } from './../card/card.component';
import { SharedModule } from '@shared/shared.module';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardComponent } from './dashboard.component';
import { ChangeViewComponent } from '../change-view/change-view.component';
import { HttpClientModule } from '@angular/common/http';
import { BaseAPIURL, AppConfig } from '../../../../../configuration';

describe('ChangeViewComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DashboardComponent, ChangeViewComponent, CardComponent],
      imports: [SharedModule, HttpClientModule],
      providers: [{ provide: BaseAPIURL, useValue: AppConfig }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
