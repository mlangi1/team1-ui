import { NgModule } from '@angular/core';

import { SharedModule } from '@shared/shared.module';

import { DashboardComponent } from './page/dashboard/dashboard.component';
import { DashboardRoutingModule } from './dashboard.routing';
import { CardComponent } from './page/card/card.component';
import { ChangeViewComponent } from './page/change-view/change-view.component';

@NgModule({
  declarations: [DashboardComponent, CardComponent, ChangeViewComponent],
  imports: [SharedModule, DashboardRoutingModule],
  exports: [],
  providers: []
})
export class DashboardModule {}
