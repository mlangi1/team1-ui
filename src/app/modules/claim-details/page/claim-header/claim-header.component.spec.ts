import { SharedModule } from './../../../../shared/shared.module';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClaimHeaderComponent } from './claim-header.component';
import { IconWorkbenchComponent } from '../icon-workbench/icon-workbench.component';
import { MatSnackBar } from '@angular/material/snack-bar';

describe('ClaimHeaderComponent', () => {
  let component: ClaimHeaderComponent;
  let fixture: ComponentFixture<ClaimHeaderComponent>;
  let snackBar: MatSnackBar;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ClaimHeaderComponent, IconWorkbenchComponent],
      imports: [SharedModule],
      providers: [MatSnackBar]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimHeaderComponent);
    component = fixture.componentInstance;
    snackBar = TestBed.inject(MatSnackBar);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('copyClaimNumber', () => {
    it('should get called when copy button is clicked', () => {
      const spyObj = spyOn(component, 'copyClaimNumber');
      const copyButton = fixture.debugElement.nativeElement.querySelector(
        '#copy-claim'
      );
      copyButton.click();
      expect(spyObj).toHaveBeenCalled();
    });
    it('should open snackbar with message that claim number has been copied', () => {
      const spyObj = spyOn(component, 'openSnackBar');
      global.document.execCommand = jest.fn();
      const copyButton = fixture.debugElement.nativeElement.querySelector(
        '#copy-claim'
      );
      copyButton.click();
      expect(spyObj).toHaveBeenCalledWith(' copied', 'DISMISS');
    });
  });

  describe('openSnackBar', () => {
    it('should open mat snackbar', () => {
      const spyObj = spyOn(snackBar, 'open');
      component.openSnackBar('testMessage', 'testAction');
      fixture.detectChanges();
      expect(spyObj).toHaveBeenCalled();
    });
  });
});
