import { Component, OnInit, Input } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { IconWorkbenchComponent } from '../icon-workbench/icon-workbench.component';

@Component({
  selector: 'app-claim-header',
  templateUrl: './claim-header.component.html',
  styleUrls: ['./claim-header.component.scss']
})
export class ClaimHeaderComponent implements OnInit {
  @Input() claimNumber: string;
  constructor(private snackBar: MatSnackBar) {}

  ngOnInit(): void {}

  copyClaimNumber(message: string, action: string) {
    const tempInput = document.createElement('input');
    document.body.appendChild(tempInput);
    tempInput.value = this.claimNumber;
    tempInput.select();
    document.execCommand('copy');
    document.body.removeChild(tempInput);
    this.openSnackBar(message, action);
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(this.claimNumber + message, action, {
      panelClass: 'dauntless-snackbar',
      duration: 2000,
      horizontalPosition: 'left',
      verticalPosition: 'bottom'
    });
  }
}
