import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedModule } from './../../../../shared/shared.module';
import { ClaimDetailsComponent } from './claim-details.component';
import { ClaimHeaderComponent } from '../claim-header/claim-header.component';
import { IconWorkbenchComponent } from '../icon-workbench/icon-workbench.component';
import { ClaimBodyComponent } from '../claim-body/claim-body.component';
import { HttpClientModule } from '@angular/common/http';
import { BaseAPIURL, AppConfig } from '../../../../../configuration';
import { ClaimsService } from '@data/service/claims.service';
import claimsDetail from '../../../../data/service/json/claim-details.json';
import { of } from 'rxjs';

describe('ClaimDetailsComponent', () => {
  let component: ClaimDetailsComponent;
  let fixture: ComponentFixture<ClaimDetailsComponent>;
  let claimService: ClaimsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ClaimDetailsComponent,
        ClaimHeaderComponent,
        ClaimBodyComponent,
        IconWorkbenchComponent
      ],
      imports: [SharedModule, HttpClientModule],
      providers: [{ provide: BaseAPIURL, useValue: AppConfig }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimDetailsComponent);
    component = fixture.componentInstance;
    claimService = TestBed.inject(ClaimsService);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch data and set service line', () => {
    const spyObj = spyOn(claimService, 'getClaimData').and.returnValue(
      of(claimsDetail)
    );
    component.ngOnInit();
    fixture.detectChanges();

    expect(spyObj).toHaveBeenCalled();
    expect(component.serviceLineData).toEqual({
      serviceLines: claimsDetail.serviceLines,
      product: claimsDetail.carrier
    });
  });
});
