import { Component, OnInit, Input } from '@angular/core';
import { ClaimHeaderComponent } from './../claim-header/claim-header.component';
import {
  ClaimDetails,
  ServiceLineTable,
  CoordinationOfBenefit
} from '@data/schema/claim-details.interface';
import { ClaimsService } from '@data/service/claims.service';

@Component({
  selector: 'app-claim-details',
  templateUrl: './claim-details.component.html',
  styleUrls: ['./claim-details.component.scss']
})
export class ClaimDetailsComponent implements OnInit {
  serviceLineData: ServiceLineTable;
  oicData: CoordinationOfBenefit[];
  claimNumber: string;
  doneLoading = false;
  constructor(private claimService: ClaimsService) {}

  ngOnInit(): void {
    this.claimService.getClaimData(111).subscribe((data: ClaimDetails) => {
      this.claimNumber = data.claimNumber;
      this.serviceLineData = {
        serviceLines: data.serviceLines,
        product: data.carrier
      };
      this.oicData = data.coordinationOfBenefits;
      this.doneLoading = true;
    });
  }
}
