import { ImageConverterService } from '@shared/service/image-converter.service';
import * as MockImage from '../../../../../assets/images/base64_Claim.json';
import { EobImageComponent } from '@shared/component/dialog/eob-image/eob-image.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-icon-workbench',
  templateUrl: './icon-workbench.component.html',
  styleUrls: ['./icon-workbench.component.scss']
})
export class IconWorkbenchComponent implements OnInit {
  image: any;

  constructor(
    private dialog: MatDialog,
    private imageConverter: ImageConverterService
  ) {}

  ngOnInit(): void {}

  openEobImage(): void {
    this.getEobImage();
    this.dialog.open(EobImageComponent, {
      ariaLabel: ' Open EOB Image',
      data: {
        blobUrl: this.image.blobUrl,
        contentType: this.image.contentType
      }
    });
  }

  getEobImage(): void {
    this.image = this.imageConverter.getDecodedImage(MockImage['pdf-test']); //use pdf-test or image to see pdf or png, use jpeg-test to see error state
  }
}
