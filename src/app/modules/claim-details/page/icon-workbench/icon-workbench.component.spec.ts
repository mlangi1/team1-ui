import { DomSanitizer } from '@angular/platform-browser';
import { ImageConverterService } from '@shared/service/image-converter.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconWorkbenchComponent } from './icon-workbench.component';
import { MatDialog } from '@angular/material/dialog';
describe('IconWorkbenchComponent', () => {
  let component: IconWorkbenchComponent;
  let fixture: ComponentFixture<IconWorkbenchComponent>;
  let imageConverterService: ImageConverterService;
  let dialog: MatDialog;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IconWorkbenchComponent],
      providers: [
        {
          provide: MatDialog,
          useValue: { open: jest.fn() }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconWorkbenchComponent);
    component = fixture.componentInstance;
    imageConverterService = TestBed.inject(ImageConverterService);
    dialog = TestBed.inject(MatDialog);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('getEobImage should call imageConverter service', () => {
    const spyObj = spyOn(imageConverterService, 'getDecodedImage').and.returnValue({
      blobUrl: 'testUrl',
      contentType: 'application/pdf'
    });
    component.getEobImage();
    fixture.detectChanges();
    expect(spyObj).toHaveBeenCalled();
  });

  it('getEobImage sets component image to imageConverter response', () => {
    const spyObj = spyOn(imageConverterService, 'getDecodedImage').and.returnValue({
      blobUrl: 'testUrl',
      contentType: 'application/pdf'
    });
    component.getEobImage();
    fixture.detectChanges();
    expect(component.image.blobUrl).toEqual('testUrl');
    expect(component.image.contentType).toEqual('application/pdf');
  });

  it('openEobImage calls getDecodedImage and dialog.open', () => {
    component.getEobImage = jest.fn();
    component.image = {
      blobUrl: 'testUrl',
      contentType: 'application/pdf'
    }
    const spyObj = spyOn(dialog, 'open');
    component.openEobImage();
    fixture.detectChanges();
    expect(component.getEobImage).toHaveBeenCalled();
    expect(spyObj).toHaveBeenCalled();
  })
});
