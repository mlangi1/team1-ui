import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClaimBodyComponent } from './claim-body.component';
import { SharedModule } from '@shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { BaseAPIURL, AppConfig } from '../../../../../configuration';
import claimsDetail from '../../../../data/service/json/claim-details.json';

describe('ClaimBodyComponent', () => {
  let component: ClaimBodyComponent;
  let fixture: ComponentFixture<ClaimBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ClaimBodyComponent],
      imports: [SharedModule, HttpClientModule],
      providers: [{ provide: BaseAPIURL, useValue: AppConfig }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimBodyComponent);
    component = fixture.componentInstance;
    component.serviceLineData = {
      serviceLines: claimsDetail.serviceLines,
      product: claimsDetail.carrier
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
