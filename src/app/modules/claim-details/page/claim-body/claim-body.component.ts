import { Component, OnInit, Input } from '@angular/core';
import { ClaimsService } from '@data/service/claims.service';
import {
  ServiceLineTable,
  CoordinationOfBenefit
} from '@data/schema/claim-details.interface';

@Component({
  selector: 'app-claim-body',
  templateUrl: './claim-body.component.html',
  styleUrls: ['./claim-body.component.scss']
})
export class ClaimBodyComponent implements OnInit {
  @Input() serviceLineData: ServiceLineTable;
  @Input() oicData: CoordinationOfBenefit[];

  serviceLineCol: string[] = [
    'serviceLine',
    'product',
    'procedures',
    'amount',
    'startDate',
    'endDate',
    'allow',
    'pay'
  ];
  oicCol: string[] = ['altPolicyNbr', 'altCarrierName', 'effectiveDt', 'endDt'];
  constructor() {}

  ngOnInit(): void {}
}
