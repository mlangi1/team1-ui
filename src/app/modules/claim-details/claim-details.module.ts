import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClaimDetailsComponent } from './page/claim-details/claim-details.component';
import { SharedModule } from '@shared/shared.module';
import { ClaimDetailsRoutingModule } from './claim-details.routing';
import { ClaimHeaderComponent } from './page/claim-header/claim-header.component';
import { IconWorkbenchComponent } from './page/icon-workbench/icon-workbench.component';
import { ClaimBodyComponent } from './page/claim-body/claim-body.component';

@NgModule({
  declarations: [
    ClaimHeaderComponent,
    ClaimDetailsComponent,
    IconWorkbenchComponent,
    ClaimBodyComponent
  ],
  imports: [CommonModule, SharedModule, ClaimDetailsRoutingModule]
})
export class ClaimDetailsModule {}
