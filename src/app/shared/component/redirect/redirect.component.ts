import {
  Component,
  OnInit,
  AfterViewChecked,
  AfterViewInit
} from '@angular/core';
import { Router } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-redirect',
  templateUrl: './redirect.component.html',
  styleUrls: ['./redirect.component.scss']
})
export class RedirectComponent implements OnInit {
  constructor(private router: Router, private oauthService: OAuthService) {}

  ngOnInit() {
    this.oauthService.events
      .pipe(filter((e) => e.type === 'token_received'))
      .subscribe((_) => {
        this.router.navigate(['/dashboard']);
      });
  }
}
