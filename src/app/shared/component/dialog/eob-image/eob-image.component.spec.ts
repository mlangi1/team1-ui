import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EobImageComponent } from './eob-image.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { SharedModule } from '@shared/shared.module';

describe('EobImageComponent', () => {
  let component: EobImageComponent;
  let fixture: ComponentFixture<EobImageComponent>;
  const data = {
    blobUrl: 'testUrl',
    contentType: 'pdf'
  };

  const errorData = {
    blobUrl: '',
    contentType: 'error/svg'
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [SharedModule],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {
            close: jest.fn()
          }
        },
        {
          provide: DomSanitizer,
          useValue: {
            bypassSecurityTrustResourceUrl: jest.fn((val) => 'safeUrl'),
            sanitize: jest.fn((val) => val)
          }
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: data
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EobImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit sets blobUrl and contentType', () => {
    expect(component.blobUrl).toEqual('safeUrl');
    expect(component.contentType).toEqual('pdf');
  });

  it('clicking the close button should close dialog', () => {
    let closeButton = fixture.debugElement.nativeElement.querySelector(
      '.close'
    );
    closeButton.click();
    expect(component.dialogRef.close).toHaveBeenCalled();
  });

  it('contentType of !error/svg renders eob-viewer', () => {
    const eobViewer = fixture.debugElement.nativeElement.querySelector(
      '.eob-viewer'
    );
    expect(eobViewer).toBeTruthy();
  });

  it('contentType of error/svg renders error-svg', () => {
    component.contentType = 'error/svg';
    fixture.detectChanges();
    const errorSvg = fixture.debugElement.nativeElement.querySelector(
      '#error-svg'
    );
    expect(errorSvg).toBeTruthy();
  });
});
