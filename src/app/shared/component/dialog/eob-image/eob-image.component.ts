import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { Component, OnInit, Inject, SecurityContext } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-eob-image',
  templateUrl: './eob-image.component.html',
  styleUrls: ['./eob-image.component.scss']
})
export class EobImageComponent implements OnInit {
  blobUrl: SafeUrl;
  contentType: string;

  constructor(
    public dialogRef: MatDialogRef<EobImageComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private sanitizer: DomSanitizer
  ) {}

  ngOnInit(): void {
    this.blobUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
      this.data.blobUrl
    );
    this.contentType = this.data.contentType;
  }

  closeDialog(): void {
    this.dialogRef.close();
  }
}
