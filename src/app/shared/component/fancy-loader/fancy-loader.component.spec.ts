import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FancyLoaderComponent } from './fancy-loader.component';

describe('FancyLoaderComponent', () => {
  let component: FancyLoaderComponent;
  let fixture: ComponentFixture<FancyLoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FancyLoaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FancyLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
