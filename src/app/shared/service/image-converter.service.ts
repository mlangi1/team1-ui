import { Injectable } from '@angular/core';
import { EobImage } from '../../data/schema/claim-image.interface';

@Injectable({
  providedIn: 'root'
})
export class ImageConverterService {
  contentType: string;
  constructor() {}

  getDecodedImage(b64Data: any, sliceSize = 512): EobImage {
    try {
      const byteCharacters = atob(b64Data.base64EncodedFile);
      const byteArrays = [];

      for (
        let offset = 0;
        offset < byteCharacters.length;
        offset += sliceSize
      ) {
        const slice = byteCharacters.slice(offset, offset + sliceSize);

        const byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
      }

      const mimeType = b64Data.mimeType;
      switch (mimeType) {
        case 'pdf':
          this.contentType = 'application/pdf';
          break;
        case 'png':
          this.contentType = 'image/png';
          break;
        default:
          this.contentType = 'error/svg';
      }
      const blob = new Blob(byteArrays, { type: this.contentType });
      const blobUrl = window.URL.createObjectURL(blob);

      return {
        blobUrl: `${blobUrl}${
          mimeType === 'pdf' ? '#toolbar=0&navpanes=0&view=fitH,100' : ''
        }`,
        contentType: this.contentType
      };
    } catch (e) {
      return {
        blobUrl: '',
        contentType: 'error/svg'
      };
    }
  }
}
