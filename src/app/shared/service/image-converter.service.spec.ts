import { TestBed } from '@angular/core/testing';

import { ImageConverterService } from './image-converter.service';
import * as MockData from '../../../assets/images/base64_Claim.json';

describe('ImageConverterService', () => {
  let service: ImageConverterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ImageConverterService);
    window.URL.createObjectURL = jest
      .fn()
      .mockImplementation((blob) => 'testUrl');
  });

  afterEach(() => {});

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getDecodedImage of PDF should return blobUrl with pdf settings and contentType of application/pdf', () => {
    const response = service.getDecodedImage(MockData['pdf-test']);
    expect(response.blobUrl).toEqual(
      'testUrl#toolbar=0&navpanes=0&view=fitH,100'
    );
    expect(response.contentType).toEqual('application/pdf');
  });

  it('getDecodedImage of PNG should return blobUrl and contentType of image/png', () => {
    const response = service.getDecodedImage(MockData['image']);
    expect(response.blobUrl).toEqual('testUrl');
    expect(response.contentType).toEqual('image/png');
  });

  it('getDecodedImage renders error SVG when receives incorrect parameters', () => {
    const response = service.getDecodedImage(MockData['does-not-exist']);
    expect(response.blobUrl).toEqual('');
    expect(response.contentType).toEqual('error/svg');
  });

  it('getDecodedImage renders error SVG when receiving file type that is not accounted for', () => {
    const response = service.getDecodedImage(MockData['jpeg-test']);
    expect(response.blobUrl).toEqual('testUrl');
    expect(response.contentType).toEqual('error/svg');
  });
});
