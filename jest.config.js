module.exports = {
  moduleNameMapper: {
    '@app/(.*)': '<rootDir>/src/app/core/$1',
    '@shared/(.*)': '<rootDir>/src/app/shared/$1',
    '@modules/(.*)': '<rootDir>/src/app/modules/$1',
    '@data/(.*)': '<rootDir>/src/app/data/$1'
  }
};
